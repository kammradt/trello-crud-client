# Welcome :tophat:

Hi, the app link is [here](https://trello-crud-client.herokuapp.com/)

## Things that need to be done:

- [ ] Create all tests
- [ ] Improve design
- [ ] Create different pages/views
- [ ] Create/Implement other HTTP Methods (PATCH, PUT, etc)
- [ ] Split App.vue functions with Vuex or simply different files
